/*!
 *	VoiceJS for Kagiso Media
 *	@author: Willem Labu
 *	@email:  willem@gloo.co.za
 *
 *	@dependancies
 *
 *		CSS: //dl.dropboxusercontent.com/u/6347389/_hosted/voice.v2.2.css
 *		      Include this file to style the plugin.. I would have done this dynamically,
 *		      but IE doesn't recognise styles like that.
 *
 *	Currently, I have hosted a modified version of jqCanvas, and the above style sheet
 *	 on my personal DropBox account. Please mirror these and host them somewhere on your own.
 *
 *	NB: I will probably take them down at some point.
 */


// Avoid `console.log` errors in browsers that lack a console.
//(function(){var e;var t=function(){};var n=["log"];var r=n.length;var i=window.console=window.console||{};while(r--){e=n[r];if(!i[e]){i[e]=t}}})();


//	Our global namescape
window.g || (window.g = {});

//	Our HTML Element's id
g.root = 'root-voice';

// The Radio Station's name
g.stationName = "Jacaranda FM";

//	Our dependancies
g.src = {

	/**
	 *	## CHANGE THIS TO YOUR OWN HOST ##
	 *	This is the callback page for SC auth.
	 */
	host: "//gloobook.co.za/soundcloud/voice", // No trailing slash


	SC: "//connect.soundcloud.com/sdk.js",
	jQuery: "//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js", // latest version in the 1.* branch
	tmpl: "//ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js", // https://github.com/BorisMoore/jquery-tmpl
	excanvas: "//explorercanvas.googlecode.com/git/excanvas.js", // https://code.google.com/p/explorercanvas/
	jqcanvas: "//dl.dropboxusercontent.com/u/6347389/_hosted/jqCanvas.min.js" // https://code.google.com/p/jqcanvas/
}

g.SC = {
	id:				"ee227b86521a64e73230bce06d81766d",
	redirect_uri:	"http://gloobook.co.za/soundcloud/voice/auth/",
	group: null, // This gets populated by JS
	groups: {
		/*
		 *	To get the group id, join the group with your SC account, and then run this in the browser console:
		 *	SC.connect(function() { SC.get('/me/groups') });
		 */
		jacarandafm: {
			id: 126877,
			url: function(){
				return "//api.soundcloud.com/groups/" + this.id // "//soundcloud.com/groups/jacarandafm"
			}
		},
		eastcoastradio: {
			id: 133436,
			url: function(){
				return "//api.soundcloud.com/groups/" + this.id // "//soundcloud.com/groups/eastcoastradio"
			}
		}
	}
}





// Called once all the JS in this file has been parsed
g.init = function(){

	// Nothing's loaded yet!
	g.isLoaded = false;

	var loadScript = function(src) {

		var root	= document.getElementById(g.root);
		var script	= document.createElement('script');
		script.type	= 'text/javascript';
		script.src	= src;

		script.onreadystatechange = g.go;
		
		// Load jQuery dependancies here.
		// ..after jQuery has loaded.
		if (src == g.src.jQuery) {
			script.onload = loadPlugins;
			script.onreadystatechange = function(){
				if (this.readyState == 'complete' || this.readyState == 'loaded') loadPlugins();
			}
		} else if (src == g.src.jqcanvas) {
			script.onload = setCanvasSupport;
			script.onreadystatechange = function(){
				if (this.readyState == 'complete' || this.readyState == 'loaded') setCanvasSupport();
			}
		} else {
			script.onload = g.go;
			script.onreadystatechange = function(){
				if (this.readyState == 'complete' || this.readyState == 'loaded') g.go();
			}
		}

		root.appendChild(script);

	}

	// Load jQuery plugins here.
	var loadPlugins = function(){
		loadScript(g.src.jqcanvas);
	}

	var setCanvasSupport = function(){
		g.canvasSupport = true;
		g.go();
	}
	
	// Explorer Canvas for IE
	if (!document.createElement('canvas').getContext) loadScript(g.src.excanvas);

	window.SC						|| loadScript(g.src.SC);
	window.jQuery && loadPlugins()	|| loadScript(g.src.jQuery);
	
	g.go();

}

/**
 *	This was grabbed from one of the SoundCloud devs' Github pages
 *	I've edited it to make it "work" (read: not throw errors) in IE 8.
 *	This is provided as is, and there are no real comments, and no guarantees.
 */
g.initSCPlugins = function() {

	// Get the jQuery.tmpl() plugin.
	$.getScript(g.src.tmpl);

	gCanvas = {
		canvas: '',
		context: '',
		width: 0,
		height: 0,
		callback: function (canvas, width, height) {

				gCanvas.canvas = canvas;
				gCanvas.context = canvas.getContext('2d');
				gCanvas.width = width;
				gCanvas.height = height;

			}
	};

	SCWaveform = function() {
		var levelInt,
			// sampling rate, adjust it for slower browsers
			frames = 25,
			rate = Math.floor(1000 / frames),
			currentTime = 0,
			// store the recorded levels in the array
			levels = [],
			// fake values smaller than 1, makes normalized waveform nicer
			enrich = function(val) {
				return val < 1 ? Math.random() : val;
			},
			// normalize values to look more like SoundCloud waveform
			normalize = function(x, max) {
				var scale = 0.25,
					normed = (Math.pow(x, scale) / Math.pow(max, scale)) * max;
				return normed;
			},
			
			// the canvas where we'll draw the waveform
			gwfCanvas = $('div.levels').jqcanvas(gCanvas.callback),

			ctx = gCanvas.context,
			ctxWidth = parseInt(ctx.canvas.width, 10),
			wfWidth = ctxWidth, // - 40,
			ctxHeight = 120, //parseInt(ctx.canvas.height, 10),

			draw = function(scaleToFit, noCounter) {

				var total = levels.length,
					// draw the time counter, use measureText()
					counterX = (scaleToFit ? wfWidth : Math.min(wfWidth, total)) + 5,
					// check if we need to scale the waveform to fit
					scale = scaleToFit && wfWidth > total ? wfWidth / total : 1;

				// clear the area
				ctx.clearRect(0, 0, ctxWidth, ctxHeight);
				
				// colour of the waveform
				ctx.fillStyle = '#555';

				// draw the waveform, make it fit in the canvas
				for (var i = 0, startOffset = Math.max(0, total - wfWidth), l = Math.min(total, wfWidth); i < l; i += 1) {
					// map the waveform amplitude
					var v = Math.round(normalize(levels[startOffset + i], ctxHeight)),
						x = Math.ceil(i * scale),
						width = Math.ceil(scale),
						// center the levels vertically
						diff = Math.round((ctxHeight - v) / 2);

					ctx.fillRect(x, diff, width, v);
				}
			},
			interpolate = function() {
				var total = levels.length,
					step = total / wfWidth,
					interpolated = [],
					avg = 0,
					i = 0,
					sauce = function() {
						var min = 0.85,
							max = 1.3,
							diff = max - min;
						return min + Math.random() * diff;
					};
				// reduce/increase the total levels to fit in the canvas
				if (step < 1) {
					// we'll stretch the waveform nicely
					var c = 0;
					while (i < wfWidth) {
						// we have too little data, so we'll add some sauce to it
						interpolated.push(levels[Math.round(c)] * sauce());
						// keep iterating
						i += 1;
						c += step;
					}
				} else {
					// we'll condense the waveform
					step = Math.floor(step);
					while (i < total) {
						// accumulate the average
						avg += levels[i];
						if (i % step === 0) {
							// find out the average value for the step
							interpolated.push(avg / step);
							avg = 0;
						}
						i += 1;
					}
				}
				return interpolated;
			},
			reset = function() {
				levels = [];
				currentTime = 0;
			};

		return {
			getLevels: function() {
				return levels;
			},
			draw: function(time, level) {
				var val = enrich(level);
				levels.push(val);
				currentTime = time;
				draw();
			},

			finishedDraw: function() {
				levels = interpolate();
				// reset the current time
				currentTime = 0;
				// render the waveform, scale if needed, no counter needed
				draw(true, true);
			},

			start: function() {
				reset();
				levelInt = setInterval(function() {
					var val = enrich(flash.api_levelIn());
					levels.push(val);
					currentTime = flash.api_bufferDuration();
					draw();
				}, rate);
			},
			stop: function() {
				// stop drawing the real-time waveform
				clearInterval(levelInt);
				levelInt = null;
			},
			reset: function() {
				reset();
				draw();
			},
			initCanvas: function() {

				var h = $('#'+g.root+' .rec-wave').height(),
					w = $('#'+g.root+' .rec-wave').width();
				
				canvas = $('div.levels').jqcanvas(gCanvas.callback);
				
				canvas.attr('width', w);
				canvas.attr('height', h);
				
				canvas = $('div.scrubber').jqcanvas(gCanvas.callback);
				canvas.attr('width', w);
				canvas.attr('height', h);
				
				wfWidth = w;
				ctxWidth = h;
			},
			render: function() {
				this.stop();
				// replace the levels with the interpolated version
				levels = interpolate();
				// reset the current time
				currentTime = 0;
				// render the waveform, scale if needed, no counter needed
				draw(true, true);
			}
		};
	}();

}

// A short snippet for detecting versions of IE in JavaScript
// without resorting to user-agent sniffing
// https://gist.github.com/padolsey/527683
g.ie = (function(){
 
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
    
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );
    
    return v > 4 ? v : undef;
    
}());


//	Check that our dependancies exist
g.go = function() {

	if (window.SC && window.jQuery && g.canvasSupport && !g.isLoaded) build();
	else return false;

	/**
	 *	Our main function.
	 *	Only called once the dependancies are available.
	 *	Wrap it in a closure to keep the global space clean.
	 */

	function build() {

		g.isLoaded = true;


		// IE
		if (g.ie) {

			$('#root-voice').addClass( 'ie ie-' + parseInt(g.ie) );
			
			// Say no to bad browsers!
			if (g.ie < 8) {
				g.noComply();
				return false;
			}
		}

		// Initialize SoundCloud!
		SC.initialize({
			client_id: g.SC.id,
			redirect_uri: g.SC.redirect_uri
		});

		/**
		 * Can't do this because IE is stupid
		 *

			$.get(g.src.css, function(data, response){
				if (response == 'success'){
					$root.prepend( $('<style />').html(data) );
				}
			});

		 */

		// Get the Group details
		SC.get('http://api.soundcloud.com/groups/' + g.SC.groups.jacarandafm.id,
			function(obj) {
				g.SC.group = obj;
			}
		);

		/**
		 *	Build the interface.
		 */
		$root = $('#' + g.root);

		// Append the contents
		$root.append(g.objects.trackTemplate);
		$root.append(g.objects.inner);
		$root.children('.inner').append(g.objects.recorder);

		// Initialise the SC Dependancies
		g.initSCPlugins();

		// Drop some JS onto the elements we've just created
		attachEvents();

		$('.spinner').hide();

	}

	function attachEvents() {

		$(".select-all").on("click", function() {

			this.select();
			return false;

		});

		setRecorded = function() {

			$('.control').hide();
			$(".play-control").show();
			$('.share').removeClass('disabled');
			$('#voice-title').attr('disabled', false);

		};


		setTimer = function(ms) {
			$(".timer").text(SC.Helper.millisecondsToHMS(ms).replace('.',':'));
		};

		$(".record-control").on("click", function(e) {

			// console.log("Track", "wantRecord");
			$('.spinner').css({ 'top': 20, 'left': 200 }).fadeIn();
			
			SC.record({

				start: function() {

					// console.log("Track", "recording");
					$('.spinner').hide();
					setTimer(0);
					SCWaveform.reset();
					$('.phase1, .phase2').toggle();
					SCWaveform.initCanvas();

					$('.control').hide();
					$(".stop-control").show();
				},

				progress: function(ms, level) {

					setTimer(ms);
					SCWaveform.draw(ms / 1000, level);
				}
			});
		});


		recordingDuration = 0;


		$(".stop-control").on("click", function(e) {

			// console.log("Track", "recorded");
			SCWaveform.finishedDraw();
			$(".reset").show();
			recordingDuration = SC.recordStop();
			setRecorded();
		});


		$(".play-control").on("click", function(e) {

			setTimer(0);
			SC.recordPlay({
				finished: setRecorded
			});

			$('.control').hide();
			$(".stop-control").show();
		});


		$("a.reset").on("click", function(e) {

			// console.log("Track", "reset");
			SC.recordStop();
			$('.share').addClass('disabled');
			$('.phase1, .phase2').toggle();
			$('#voice-title').val('');
			$('.control').hide();
			$(".record-control").show();
			setTimer(0);
			$(this).hide();

			e.preventDefault();

		});

		$("a.share").on("click", function(e) {
			
			if ($(this).hasClass("disabled")) {
				return false;
			}

			// console.log("Track", "wantUpload");

			if ($('#voice-title').val() == '' || $('#voice-title').val() == 'Please enter a title') {

				$('#voice-title').val('Please enter a title').css('color', '#c00').select();
				setTimeout(function(){
					$('#voice-title').css('color', '#555');
				}, 4000);
				return false;

			}

			SC.connect({

				redirect_uri: g.SC.redirect_uri,

				connected: function() {

					var $root = $('#'+g.root),
						$track,
						tags,
						track,
						trackParams;

					// console.log("Track", "connected");

					track = {
						title: $("#voice-title").val(),
						sharing: "public",
						downloadable: true,
						shared_to: {
							connections: [{
								no: 0
							}]
						}
					};

					trackParams = {
						track: track
					};

					tags = '"' + g.stationName + '"'; // Wrap multi-word tags in '"'

					$.extend(track, {
						state: "uploading",
						user: {
							username: ""
						},
						tag_list: tags,
						duration: 0,
						permalink_url: ""
					});


					$track = $("#trackTmpl").tmpl(track).appendTo($root).addClass("unfinished");
					$track.find(".status").text("Uploading...");


					SC.recordUpload(trackParams, function(track) {

						var checkState;

						// console.log('track', track);

						// console.log("Track", "upload");
						$track = $("#trackTmpl").tmpl(track).replaceAll($track).addClass("unfinished");

						checkState = function() {

							SC.get(track.uri, function(track) {

								// console.log('checkState track', track);

								if (track.state === "finished") {

									$track.find(".status").text("Queued for moderation.");
									$track.removeClass("unfinished");

								} else {

									window.setTimeout(checkState, 3000);
								}

							});

						};

						window.setTimeout(checkState, 3000);

						$root.addClass("recorded-track");

						SC.put(g.SC.group.uri + "/contributions/" + track.id, function() {

							// console.log("Track", "contributed");

						});
					});
				}
			});

			e.preventDefault();

		});

		// IE doesn't like placeholders
		if (g.ie && g.ie < 10) {
			$("#voice-title").val("Your sound's title");
		}

	} // end attachEvents()

}

// No canvas support for lt IE8
g.noComply = function(){
	$('#'+g.root).html('<h4>Please update your browser to record and share your sounds.</h4>');
}

// Initialise the code
g.init();

/*
 *	Our HTML objects
 */
g.objects = {

	inner:
		'<div class="inner">' +
		'	<div class="spinner"></div>' +
		'</div>',

	recorder:
		'<div class="recorder clearfix">' +
			'<div class="left">' +
				'<div class="controls-container">' +
					'<div class="controls">' +
						'<div title="Play" class="control play-control"></div>' +
						'<div title="Pause" class="control pause-control"></div>' +
						'<div title="Stop" class="control stop-control"></div>' +
						'<div title="Record" class="control record-control"></div>' +
						'<div class="mic"></div>' +
						'<div class="footer"></div>' +
					'</div>' +
				'</div>' +
			'</div>' +
			'<div class="right">' +
				'<div class="phase1">' +
					'<h2>Record &amp; Share</h2>' +
					'<p>Click record and allow access to your microphone to start recording your own sound and share your opinions, jokes or comments with ' + g.stationName + '!</p>' +
				'</div>' +
				'<div class="phase2">' +
					'<div class="timer"></div>' +
					'<div class="rec-wave-container">' +
						'<a class="reset" href="#" title="Reset">' +
							'<span class="reset-control"></span>' +
						'</a>' +
						'<div class="rec-wave">' +
							'<div class="levels"></div>' +
							'<div class="scrubber"></div>' +
						'</div>' +
					'</div>' +
					'<div class="bottom">' +
						'<input name="title" id="voice-title" type="text" placeholder="Your sound\'s title" title="Title" value="" class="select-all">' +
						'<a class="share disabled" href="#">Share</a>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>',

	trackTemplate:
		'<script id="trackTmpl" type="text/x-jquery-tmpl">' +
			'<div class="track">' +
				'<a href="${permalink_url}" data-trackId="${id}" class="trackLink">' +
					'<div class="track-details">' +
						'<span class="track-title">${title}</span> by <span class="track-user">${user.username}</span>' +
						'<span class="status">Processing...</span>' +
					'</div>' +
				'</a>' +
			'</div>' +
		'</script>'
}
