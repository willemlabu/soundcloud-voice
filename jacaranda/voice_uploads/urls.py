from django.conf.urls import patterns, url

from voice_uploads import views

urlpatterns = patterns('',
	# our root
    url(r'^$', views.index, name='index'),

    # authorising callback from soundcloud
    url(r'^auth/$', views.auth, name='auth'),

    # Once authed, show things
    url(r'^user/$', views.user, name='user')
)