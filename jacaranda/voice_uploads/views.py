from django.http import HttpResponse
from django.http import HttpResponseRedirect

from django.template import RequestContext, loader

import soundcloud

CLIENT_ID     = 'ee227b86521a64e73230bce06d81766d'
CLIENT_SECRET = 'f27d7da08248ea3f2b62cebd9defdfcf'
REDIRECT_URL  = 'http://gloobook.co.za:8000/soundcloud/voice/auth/'

GROUP_ID      = 126877 # Jacaranda
# GROUP_ID    = 133436 # East Coast

# The index page - just force a redirect for now
def index(request):

    # create client object with app credentials
    client    = soundcloud.Client( client_id     = CLIENT_ID    ,
                                   client_secret = CLIENT_SECRET,
                                   redirect_uri  = REDIRECT_URL   )

    # redirect user to authorize URL
    return HttpResponseRedirect(client.authorize_url())



def auth(request):
    
    # create client object with app credentials
    client    = soundcloud.Client( client_id     = CLIENT_ID    ,
                                   client_secret = CLIENT_SECRET,
                                   redirect_uri  = REDIRECT_URL   )

    # exchange authorization code for access token
    code         = request.GET.get('code', '')
    access_token = client.exchange_token(code)

    # we should actually save this access token in the db - connected to the user.
    request.session['access_token'] = access_token.access_token

    return HttpResponse('<a href="../user">Upload file</a>')



def user(request):

    # Use the access token instead of going through the whole process
    client       = soundcloud.Client(access_token = request.session['access_token'])

    # make an authenticated call
    user         = client.get('/me')

    # upload audio file
    upload       = client.post('/tracks', track={
        'title': 'This is an uploaded sound',
        'asset_data': open('C:/xampp/htdocs/soundcloud/audio.wav', 'rb')
    })

    group        = client.post('/groups/%d/contributions' % (GROUP_ID,), track={
        'id': upload.id
    })

    # Our page template
    template     = loader.get_template('index.html')

    # Set the variables we'll use in the page
    context      = RequestContext(request, {
        'user': user,
        'track': upload.id
    })

    return HttpResponse(template.render(context))